package email

import (
	"errors"
	"fmt"
	"net/mail"
	"net/smtp"
	"strings"
)

type EmailSession struct {
	SmtpHost  string
	SmtpPort  string
	FromText  string
	FromEmail string
	BccEmail  string
}

//
// Create new email session instance setting default parameters
//
// PROD
//host := "smtp.wiscmail.wisc.edu"
//port := "25"
//
// DEV
// test with ssh tunnel crated as follows:
// ssh -L 8025:smtp.wiscmail.wisc.edu:25 jh9@crate.doit.wisc.edu
//host := "localhost"
//port := "8025"
//
// Example:
// ems := NewEmailSession("smtp.wiscmail.wisc.edu", "25", UW Madison Box Admin", "box-admins@lists.wisc.edu", "")
// ems.SendHtmlEmailMsg("netid@wisc.edu", "test email", "test body message")
//
func NewEmailSession(host, port, frText, frEmail, bccEmail string) *EmailSession {
	s := &EmailSession{
		SmtpHost:  host,
		SmtpPort:  port,
		FromText:  frText,
		FromEmail: frEmail,
		BccEmail:  bccEmail,
	}
	return s
}

func (this *EmailSession) SendHtmlEmailMsg(to, subj, body string) error {
	x := make([]string, 1)
	x[0] = to
	return this.SendHtmlEmailMsgToMult(x, subj, body)
}

func (this *EmailSession) encodeRFC2047(String string) string {
	// use mail's rfc2047 to encode any string
	addr := mail.Address{String, ""}
	return strings.Trim(addr.String(), " <>")
}

func (this *EmailSession) SendHtmlEmailMsgToMult(to []string, subj, body string) error {

	from := mail.Address{this.FromText, this.FromEmail}

	message := ""
	message += fmt.Sprintf("From: %s\r\n", from.String())
	message += fmt.Sprintf("To: %s\r\n", to)
	if this.BccEmail != "" {
		message += fmt.Sprintf("Bcc: %s\r\n", this.BccEmail)
	}
	message += fmt.Sprintf("Subject: %s\r\n", subj)
	message += "MIME-Version: 1.0\r\n"
	message += "Content-Type: text/html; charset=\"UTF-8\"\r\n"
	message += "\r\n"
	message += body
	message += "\r\n"

	// Connect to the remote SMTP server.
	c, err := smtp.Dial(this.SmtpHost + ":" + this.SmtpPort)
	if err != nil {
		return errors.New(fmt.Sprintf("smtp.Dial failed with %s", err))
	} else {
		defer c.Close()

		if err = c.Mail(from.Address); err != nil {
			return errors.New(fmt.Sprintf("c.Mail failed with %s", err))
		}

		for _, adx := range to {
			if err := c.Rcpt(adx); err != nil {
				return errors.New(fmt.Sprintf("c.Rcpt failed with %s", err))
			}
		}

		w, err := c.Data()
		if err != nil {
			return errors.New(fmt.Sprintf("c.Data failed with %s", err))
		}

		_, err = w.Write([]byte(message))
		if err != nil {
			return errors.New(fmt.Sprintf("w.Write failed with %s", err))
		}

		err = w.Close()
		if err != nil {
			return errors.New(fmt.Sprintf("w.Close failed with %s", err))
		}
		c.Quit()
	}
	return nil
}
