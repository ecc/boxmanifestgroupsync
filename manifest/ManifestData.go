package manifest

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
)

type ManifestData struct {
	//GroupDataFile           string
	//GroupMembershipDataFile string
	DbCmd            string
	DbSvc            string
	DbHost           string
	DbPort           string
	DbUser           string
	DbPass           string
	Groups           []string
	GroupMemberships []GroupMembership
}

func (m *ManifestData) LoadGroupData() error {

	args := []string{"GROUPS"}
	rval, err := RunManifestFetchCommand(m.DbCmd, args, m.DbSvc, m.DbHost, m.DbPort, m.DbUser, m.DbPass)
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		return err
	}
	lines := strings.Split(rval, "\n")
	firstLine := true
	for _, line := range lines {
		// filter out empty lines
		line = strings.TrimSpace(line)
		if line != "" {
			//fmt.Printf("grp: %s\n", line)

			if firstLine {
				firstLine = false
			} else {
				m.Groups = append(m.Groups, line)
			}
		}
	}
	return nil
}

func (m *ManifestData) LoadGroupMembershipData() error {

	args := []string{"MEMBERS"}
	rval, err := RunManifestFetchCommand(m.DbCmd, args, m.DbSvc, m.DbHost, m.DbPort, m.DbUser, m.DbPass)
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		return err
	}
	lines := strings.Split(rval, "\n")
	firstLine := true
	for _, line := range lines {
		// filter out empty lines
		line = strings.TrimSpace(line)
		if line != "" {
			//fmt.Printf("mbr: %s\n", line)

			if firstLine {
				firstLine = false
			} else {
				tokens := strings.Split(line, ",")
				if len(tokens) == 2 {
					mb := &GroupMembership{
						GroupName: tokens[0],
						Eppn:      tokens[1],
					}
					m.GroupMemberships = append(m.GroupMemberships, *mb)
				}
			}
		}
	}
	return nil
}

func (m *ManifestData) LoadData() error {

	err := m.LoadGroupData()
	if err != nil {
		return err
	}

	err = m.LoadGroupMembershipData()
	if err != nil {
		return err
	}

	return nil
}

func RunManifestFetchCommand(cmd string, args []string, svc, host, port, user, pass string) (string, error) {
	binary, err := exec.LookPath(cmd)
	if err != nil {
		return "", err
	}

	execCmd := exec.Command(binary, args...)
	env := os.Environ()
	env = append(env, fmt.Sprintf("DB_SVC=%s", svc))
	env = append(env, fmt.Sprintf("DB_HOST=%s", host))
	env = append(env, fmt.Sprintf("DB_PORT=%s", port))
	env = append(env, fmt.Sprintf("DB_USER=%s", user))
	env = append(env, fmt.Sprintf("DB_PASS=%s", pass))
	execCmd.Env = env

	cmdOut, _ := execCmd.StdoutPipe()
	cmdErr, _ := execCmd.StderrPipe()

	startErr := execCmd.Start()
	if startErr != nil {
		return "", startErr
	}

	// read stdout and stderr
	stdOutput, _ := ioutil.ReadAll(cmdOut)
	errOutput, _ := ioutil.ReadAll(cmdErr)

	//fmt.Printf("STDOUT: %s\n", stdOutput)
	//fmt.Printf("ERROUT: %s\n", errOutput)

	err = execCmd.Wait()
	if err != nil {
		return string(stdOutput), errors.New(fmt.Sprintf("%s %v", errOutput, err))
	}
	return string(stdOutput), nil
}
