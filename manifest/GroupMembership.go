package manifest

type GroupMembership struct {
	GroupName string
	Eppn      string
}
