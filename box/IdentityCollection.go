package box

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"strconv"
)

type IdentityCollection struct {
	Count  int        `json:"total_count,omitempty"`
	Entry  []Identity `json:"entries,omitempty"`
	Limit  int        `json:"limit,omitempty"`
	Offset int        `json:"offset,omitempty"`
}

func (this *IdentityCollection) Groups(box *Box) error {

	rawurl := fmt.Sprintf("groups")
	body, err := box.doRequest("GET", rawurl, nil, nil)
	//fmt.Println("IdentityCollection.Groups: body=" + string(body[:]))
	if err == nil {
		err = json.Unmarshal(body, this)
		return err
	}
	return err
}

func (this *IdentityCollection) GetGroupIdByName(box *Box, groupName string) error {

	//fmt.Println("GetGroupIdByName: groupName=" + groupName)
	if groupName == "" {
		return errors.New("Empty groupName while using GetGroupIdByName")
	}
	params := &url.Values{"filter_term": {groupName}}
	rawurl := fmt.Sprintf("groups")
	body, err := box.doRequest("GET", rawurl, params, nil)
	//fmt.Println("IdentityCollection.GetGroupIdByName: body=" + string(body[:]))
	if err == nil {
		err = json.Unmarshal(body, this)
		return err
	}
	return err
}

func (this *IdentityCollection) GetUserIdByLogin(box *Box, userLogin string) error {

	//fmt.Sprintln("GetUserIdByLogin: userLogin=" + userLogin)
	if userLogin == "" {
		return errors.New("Empty userLogin while using GetUserIdByLogin")
	}
	params := &url.Values{"filter_term": {userLogin}}
	rawurl := fmt.Sprintf("users")
	body, err := box.doRequest("GET", rawurl, params, nil)
	//fmt.Println("IdentityCollection.GetUserIdByLogin: body=" + string(body[:]))
	if err == nil {
		err = json.Unmarshal(body, this)
		return err
	}
	return err
}

func (this *IdentityCollection) GetUsersFilteredBy(box *Box, filterText string, limit, offset int) error {
	if filterText == "" {
		return errors.New("Empty filterText while using GetUsersFilteredBy")
	}
	// Pass in the Limit and Offset so we can handle the case where the result set is greater than 1000 records
	params := &url.Values{"filter_term": {filterText}, "limit": {strconv.Itoa(limit)}, "offset": {strconv.Itoa(offset)}}
	rawurl := fmt.Sprintf("users")
	body, err := box.doRequest("GET", rawurl, params, nil)
	if err == nil {
		err = json.Unmarshal(body, this)
		return err
	}
	return err
}
