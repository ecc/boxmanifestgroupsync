package box

import (
	"errors"
)

//  Represents both mini user and mini group.
type Identity struct {
	Name  string `json:"name,omitempty"`  // The name of the entity.
	Id    string `json:"id,omitempty"`    // The id of the entity.
	Login string `json:"login,omitempty"` // The email id of user
	Type  string `json:"type,omitempty"`  // Type of entity
}

// Checks if the given identity is a group
func (this *Identity) IsGroup() bool {
	if this.Type == "group" {
		return true
	} else {
		return false
	}
}

// Converts the given idenity to a group. Only attributes present in
// the entity are populated rest are untouched.
func (this *Identity) toGroup(g *Group) error {
	if !this.IsGroup() {
		return errors.New("Idenity is not a group")
	}
	g.Id = this.Id
	g.Name = this.Name
	g.Type = this.Type
	return nil
}

// Checks if the given idenity is a user.
func (this *Identity) IsUser() bool {
	if this.Type == "user" {
		return true
	} else {
		return false
	}
}

// toUser converts the given idenity to a user. Only attributes present in
// the entity are populated rest are untouched.
func (this *Identity) toUser(u *User) error {
	if !this.IsUser() {
		return errors.New("Entity is not a user")
	}
	u.Id = this.Id
	u.Name = this.Name
	u.Type = this.Type
	u.Login = this.Login
	return nil
}
