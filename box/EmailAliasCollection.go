package box

import (
	"encoding/json"
	"errors"
	"fmt"
)

type EmailAliasCollection struct {
	Count  int          `json:"total_count,omitempty"`
	Entry  []EmailAlias `json:"entries,omitempty"`
	Limit  int          `json:"limit,omitempty"`
	Offset int          `json:"offset,omitempty"`
}

// get email alliases for a given box user id
func (this *EmailAliasCollection) GetByUserID(box *Box, userID string) error {

	fmt.Sprintln("GetByUserID: userID=" + userID)
	if userID == "" {
		return errors.New("Empty userID while using GetByUserID")
	}
	rawurl := fmt.Sprintf("users/%s/email_aliases", userID)
	body, err := box.doRequest("GET", rawurl, nil, nil)
	fmt.Println("EmailAliasCollection.GetByUserID: body=" + string(body[:]))
	if err == nil {
		err = json.Unmarshal(body, this)
		return err
	}
	return err
}

// scans thru the entries looking for a matching email and returns the email alias id
// which is needed for the delete step
func (this *EmailAliasCollection) LookupEmailAliasIDByEmail(email string) string {

	for _, entry := range this.Entry {
		if email == entry.Email {
			return entry.Id
		}
	}
	return ""
}
