package box

type BoxLock struct {
	Id        string   `json:"id,omitempty"`
	CreatedBy string   `json:"created_by,omitempty"`
	CreatedAt *BoxTime `json:"created_at,omitempty"`
	ExpiresAt *BoxTime `json:"expires_at,omitempty"`
	Download  bool     `json:"is_download_prevented,omitempty"`
}
