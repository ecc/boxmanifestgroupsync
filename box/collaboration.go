package box

import (
	"encoding/json"
	"errors"
	"fmt"
)

type Collaboration struct {
	Type           string    `json:"type,omitempty"`
	Id             string    `json:"id,omitempty"`
	CreatedBy      *Identity `json:"created_by,omitempty"` // mini user object
	CreatedAt      *BoxTime  `json:"created_at,omitempty"`
	ModifiedAt     *BoxTime  `json:"modified_at,omitempty"`
	Status         string    `json:"status,omitempty"`        // 'accepted', 'pending' or 'rejected'
	AccessibleBy   *Identity `json:"accessible_by,omitempty"` // mini user or group object
	Role           string    `json:"role,omitempty"`          // 'editor', 'viewer', 'previewer', 'uploader', 'viewer uploader', 'co-owner' or 'owner'
	AcknowledgedAt *BoxTime  `json:"acknowledged_at,omitempty"`
	Item           *Entity   `json:"item,omitempty"` // mini folder object, folder this collaboration is related to
	Notify         bool      `json:"notify,omitempty"`
	Fields         string    `json:"fields,omitempty"`
}

// Get populates the fields of the struct.
// Only Id is required apriori.
func (this *Collaboration) Get(box *Box) error {
	if this.Id == "" {
		return errors.New("Empty id while using Get")
	}
	rawurl := fmt.Sprintf("collaborations/%s", this.Id)
	//fmt.Println("collaboratoin.Get: rawurl=" + rawurl)
	body, err := box.doRequest("GET", rawurl, nil, nil)

	if err == nil {
		err = json.Unmarshal(body, this)
		return err
	}
	return err
}

// Required fields: Item, Id, Type, AccessibleBy and Role
func (this *Collaboration) CreateCollaboration(box *Box) error {
	// Name should be set when CreateCollaboration is called
	reqBody, _ := json.Marshal(this)

	body, err := box.doRequest("POST", "collaborations", nil, reqBody)
	if err == nil {
		err = json.Unmarshal(body, this)
		return err
	}
	return err
}

// Id should be set before calling DeleteCollaboration
func (this *Collaboration) DeleteCollaboration(box *Box) error {
	rawurl := fmt.Sprintf("collaborations/%s", this.Id)
	_, err := box.doRequest("DELETE", rawurl, nil, nil)
	return err
}
