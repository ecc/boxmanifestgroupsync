package box

type Permission struct {
	Download bool `json:"can_download,omitempty"`
	Preview  bool `json:"can_preview,omitempty"`
	Upload   bool `json:"can_upload,omitempty"`
	Comment  bool `json:"can_comment,omitempty"`
	Rename   bool `json:"can_rename,omitempty"`
	Delete   bool `json:"can_delete,omitempty"`
	Share    bool `json:"can_share,omitempty"`
	SetShare bool `json:"can_set_share_access,omitempty"`
}
