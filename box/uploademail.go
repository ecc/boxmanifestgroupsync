package box

type UploadEmail struct {
	Access string `json:"access,omitempty"`
	Email  string `json:"email,omitempty"`
}
