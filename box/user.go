package box

import (
	"encoding/json"
	"errors"
	"fmt"
)

type User struct {
	Type          string   `json:"type,omitempty"`
	Id            string   `json:"id,omitempty"`
	Name          string   `json:"name,omitempty"`
	Login         string   `json:"login,omitempty"`
	CreatedAt     *BoxTime `json:"created_at,omitempty"`
	ModifiedAt    *BoxTime `json:"modified_at,omitempty"`
	Language      string   `json:"language,omitempty"`
	timezone      string   `json:"timezone,omitempty"`
	SpaceAmount   float64  `json:"space_amount,omitempty"`
	SpaceUsed     float64  `json:"space_used,omitempty"`
	MaxUploadSize float64  `json:"max_upload_size,omitempty"`
	Status        string   `json:"status,omitempty"`
	JobTitle      string   `json:"job_title,omitempty"`
	Phone         string   `json:"phone,omitempty"`
	Address       string   `json:"address,omitempty"`
	AvatarUrl     string   `json:"avatar_url,omitempty"`
	Role          string   `json:"role,omitempty"`
	// TrackingCodes
	CanSeeManagedUsers            bool `json:"can_see_managed_users,omitempty"`
	IsSyncEnabled                 bool `json:"is_sync_enabled,omitempty"`
	IsExternalCollabRestricted    bool `json:"is_external_collab_restricted,omitempty"`
	IsExemptFromDeviceLimits      bool `json:"is_exempt_from_device_limits,omitempty"`
	IsExemptFromLoginVerification bool `json:"is_exempt_from_login_verification,omitempty"`
	// Enterprise
	MyTags   []string `json:"my_tags,omitempty"`
	Hostname string   `json:"hostname,omitempty"`
}

// Get populates the fields of the struct.
// Only Id is required apriori.
func (this *User) Get(box *Box) error {
	if this.Id == "" {
		return errors.New("Empty id while using Get")
	}
	rawurl := fmt.Sprintf("users/%s", this.Id)
	//fmt.Printf("user.Get: rawurl=%s\n", rawurl)
	body, err := box.doRequest("GET", rawurl, nil, nil)

	if err == nil {
		err = json.Unmarshal(body, this)
		return err
	}
	return err
}

// Update user object
// Id and field-to-update must be set before calling
func (this *User) Update(box *Box) error {
	if this.Id == "" {
		return errors.New("Empty id while using Update")
	}
	reqBody, _ := json.Marshal(this)
	rawurl := fmt.Sprintf("users/%s", this.Id)
	//beego.Debug("------------ User.Update " + rawurl)
	//beego.Debug("------------ User.Update " + string(reqBody))
	body, err := box.doRequest("PUT", rawurl, nil, reqBody)
	if err == nil {
		err = json.Unmarshal(body, this)
		return err
	}
	return err
}

// Name should be set before calling this
func (this *User) CreateUser(box *Box) error {
	// Login, Name should be set when CreateUser is called
	reqBody, _ := json.Marshal(this)

	body, err := box.doRequest("POST", "users", nil, reqBody)
	if err == nil {
		err = json.Unmarshal(body, this)
		return err
	}
	return err
}
