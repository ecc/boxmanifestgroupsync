package box

import (
	"encoding/json"
	"errors"
	"fmt"
)

type Group struct {
	Type                   string   `json:"type,omitempty"`
	Id                     string   `json:"id,omitempty"`
	Name                   string   `json:"name,omitempty"`
	CreatedAt              *BoxTime `json:"created_at,omitempty"`
	ModifiedAt             *BoxTime `json:"modified_at,omitempty"`
	Provenance             string   `json:"provenance,omitempty"`
	ExternalSyncId         string   `json:"external_sync_identifier,omitempty"`
	Description            string   `json:"description,omitempty"`
	InvitabilityLevel      string   `json:"invitability_level,omitempty"`
	MemberViewabilityLevel string   `json:"member_viewability_level,omitempty"`
}

// Get populates the fields of the struct.
// Only Id is required apriori.
func (this *Group) Get(box *Box) error {
	if this.Id == "" {
		return errors.New("Empty id while using Get")
	}
	rawurl := fmt.Sprintf("groups/%s", this.Id)
	//fmt.Println("group.Get: rawurl=" + rawurl)
	body, err := box.doRequest("GET", rawurl, nil, nil)

	if err == nil {
		err = json.Unmarshal(body, this)
		return err
	}
	return err
}

// Name should be set before calling this
func (this *Group) CreateGroup(box *Box) error {
	// Name should be set when CreateGroup is called
	reqBody, _ := json.Marshal(this)

	body, err := box.doRequest("POST", "groups", nil, reqBody)
	if err == nil {
		err = json.Unmarshal(body, this)
		return err
	}
	return err
}

// Id should be set before calling DeleteGroup
func (this *Group) DeleteGroup(box *Box) error {
	rawurl := fmt.Sprintf("groups/%s", this.Id)
	_, err := box.doRequest("DELETE", rawurl, nil, nil)
	return err
}
