package box

import (
	"encoding/json"
	"errors"
	"fmt"
)

type EmailAlias struct {
	Id          string `json:"id,omitempty"`    // The id of email alias
	Email       string `json:"email,omitempty"` // The email id of user
	Type        string `json:"type,omitempty"`  // Set to: "email_alias"
	IsConfirmed bool   `json:"is_confirmed,omitempty"`
}

// create an email alias for the given userid
// Email must be set apriori
func (this *EmailAlias) CreateForUserID(box *Box, userID string) error {

	this.Type = "email_alias"

	if userID == "" {
		return errors.New("Empty userID while using CreateForUserID")
	}
	reqBody, _ := json.Marshal(this)
	rawurl := fmt.Sprintf("users/%s/email_aliases", userID)
	//fmt.Printf("user.Get: rawurl=%s\n", rawurl)
	body, err := box.doRequest("POST", rawurl, nil, reqBody)

	if err == nil {
		err = json.Unmarshal(body, this)
		return err
	}
	return err
}

// The Id must be set apriori (the email_alias_id)
func (this *EmailAlias) DeleteEmailAliasForUserID(box *Box, userID string) error {
	rawurl := fmt.Sprintf("users/%s/email_aliases/%s", userID, this.Id)
	_, err := box.doRequest("DELETE", rawurl, nil, nil)
	return err
}
