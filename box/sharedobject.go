package box

type SharedObject struct {
	Url           string      `json:"url,omitempty"`
	DownloadUrl   string      `json:"download_url,omitempty"`
	VanityUrl     string      `json:"vanity_url,omitempty"`
	HasPassword   bool        `json:"is_password_enabled,omitempty"`
	UnsharedAt    *BoxTime    `json:"unshared_at,omitempty"`
	DownloadCount int         `json:"download_count,omitempty"`
	PreviewCount  int         `json:"preview_count,omitempty"`
	Access        string      `json:"access,omitempty"`
	Permission    *Permission `json:"permissions,omitempty"`
}
