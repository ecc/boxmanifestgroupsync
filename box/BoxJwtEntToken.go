package box

import (
	"crypto/rsa"
	_ "encoding/json"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"io/ioutil"
	"time"
)

type BoxJwtEntToken struct {
	PrivateKeyFile string
	PublicKeyFile  string
	TokenString    string
}

func NewBoxJwtEntToken(privKeyFile, pubKeyFile string) *BoxJwtEntToken {
	t := &BoxJwtEntToken{
		PrivateKeyFile: privKeyFile,
		PublicKeyFile:  pubKeyFile,
		TokenString:    "",
	}
	return t
}

func (this *BoxJwtEntToken) getPrivateKey(f string) (*rsa.PrivateKey, error) {
	var privKey *rsa.PrivateKey

	key_data, err := ioutil.ReadFile(f)
	if err != nil {
		return nil, err
	}

	privKey, err = jwt.ParseRSAPrivateKeyFromPEM(key_data)
	if err != nil {
		return nil, err
	}

	return privKey, nil
}

func (this *BoxJwtEntToken) getPublicKey(f string) (*rsa.PublicKey, error) {
	var pubKey *rsa.PublicKey

	key_data, err := ioutil.ReadFile(f)
	if err != nil {
		return nil, err
	}

	pubKey, err = jwt.ParseRSAPublicKeyFromPEM(key_data)
	if err != nil {
		return nil, err
	}

	return pubKey, nil
}

func (this *BoxJwtEntToken) GenerateEnterpriseToken(keyId, clientId, enterpriseId, uniqueId string) (string, error) {

	private_key, err := this.getPrivateKey(this.PrivateKeyFile)
	if err != nil {
		return "", err
	}

	type BoxCustomClaims struct {
		BoxSubType string `json:"box_sub_type"`
		jwt.StandardClaims
	}

	claims := BoxCustomClaims{
		"enterprise",
		jwt.StandardClaims{
			Issuer:    clientId,
			Subject:   enterpriseId,
			Audience:  "https://api.box.com/oauth2/token",
			Id:        uniqueId,
			ExpiresAt: time.Now().Add(time.Minute * 1).Unix(),
		},
	}

	t := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	t.Header["kid"] = keyId
	this.TokenString, err = t.SignedString(private_key)
	if err != nil {
		this.TokenString = ""
		return "", err
	}

	return this.TokenString, nil
}

func (this *BoxJwtEntToken) VerifyEnterpriseToken() (bool, error) {

	public_key, err := this.getPublicKey(this.PublicKeyFile)
	if err != nil {
		fmt.Printf("Error Getting Public Key: %s", err)
	}

	type BoxCustomClaims struct {
		BoxSubType string `json:"box_sub_type"`
		jwt.StandardClaims
	}

	token, err := jwt.ParseWithClaims(this.TokenString, &BoxCustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		return public_key, nil
	})
	if err != nil {
		return false, err
	}

	if !token.Valid {
		return false, errors.New("Token not valid.")
	}

	clms, ok := token.Claims.(*BoxCustomClaims)
	if !ok {
		return false, errors.New("Unable to reference token claims")
	}

	//	fmt.Printf("BoxSubType: %v\n", clms.BoxSubType)
	//	fmt.Printf("ExpiresAt: %v\n", clms.StandardClaims.ExpiresAt)
	if clms.StandardClaims.VerifyExpiresAt(time.Now().Unix(), true) {
		return true, nil
	} else {
		return false, errors.New("Token Expired")
	}
}
