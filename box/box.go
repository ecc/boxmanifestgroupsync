//
// box.com REST API
//
// Implements Box.com API 2.0 https://developers.box.com/docs/
//
//
package box

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	_ "net/http/httputil"
	"net/url"
	"strings"
	"time"

	"golang.org/x/net/context"
	"golang.org/x/oauth2"
)

const (
	AuthURL      = "https://app.box.com/api/oauth2/authorize"
	TokenURL     = "https://app.box.com/api/oauth2/token"
	ApiURL       = "https://api.box.com/2.0"
	AipUploadURL = "https://upload.box.com/api/2.0"
)

type AuthTransport struct {
	bx *Box
	rt http.RoundTripper
}

func (t *AuthTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	if req.URL.String() == TokenURL {
		// this is a token refresh request
		resp, e := t.rt.RoundTrip(req)

		body, err := ioutil.ReadAll(io.LimitReader(resp.Body, 1<<20))
		if err == nil {
			if c := resp.StatusCode; c >= 200 && c <= 299 {
				type TokenResponse struct {
					TokenType    string `json:"token_type"`
					RefreshToken string `json:"refresh_token"`
					AccessToken  string `json:"access_token"`
					ExpiresIn    int64  `json:"expires_in"` // relative seconds
				}

				var tokenResp TokenResponse
				if err := json.Unmarshal(body, &tokenResp); err == nil {
					token := &oauth2.Token{
						AccessToken:  tokenResp.AccessToken,
						RefreshToken: tokenResp.RefreshToken,
						TokenType:    tokenResp.TokenType,
					}
					if secs := tokenResp.ExpiresIn; secs > 0 {
						token.Expiry = time.Now().Add(time.Duration(secs) * time.Second)
					}
					t.bx.StoreToken(token)

					// reset the body so the oauth2 code picks up the new tokens
					resp.Body = ioutil.NopCloser(bytes.NewBuffer(body))
					resp.ContentLength = int64(len(body))
				}
			}
		}
		return resp, e
	}
	// just pass everything else through
	return t.rt.RoundTrip(req)
}

type Box struct {
	ApiUrl                   string
	ApiUploadUrl             string
	SuppressBoxNotifications bool
	OnBehalfOf               string
	AsUser                   string
	Config                   *oauth2.Config
	StoredToken              *oauth2.Token
}

func NewBoxWithToken(ClientId, ClientSecret, RedirectURL string, t *oauth2.Token) *Box {
	b := &Box{
		ApiUrl:                   ApiURL,
		ApiUploadUrl:             AipUploadURL,
		SuppressBoxNotifications: false,
		OnBehalfOf:               "",
		AsUser:                   "",
	}
	b.Config = &oauth2.Config{
		ClientID:     ClientId,
		ClientSecret: ClientSecret,
		RedirectURL:  RedirectURL,
		Endpoint: oauth2.Endpoint{
			AuthURL:  AuthURL,
			TokenURL: TokenURL,
		},
	}
	b.StoredToken = t

	return b
}

func NewBoxNoToken(ClientId, ClientSecret, RedirectURL string) *Box {
	b := &Box{
		ApiUrl:                   ApiURL,
		ApiUploadUrl:             AipUploadURL,
		SuppressBoxNotifications: false,
		OnBehalfOf:               "",
		AsUser:                   "",
	}
	b.Config = &oauth2.Config{
		ClientID:     ClientId,
		ClientSecret: ClientSecret,
		RedirectURL:  RedirectURL,
		Endpoint: oauth2.Endpoint{
			AuthURL:  AuthURL,
			TokenURL: TokenURL,
		},
	}
	return b
}

func (b *Box) JwtRequestEntToken(clientId, clientSecret, jwtAssersion string) (string, string, int64, error) {

	var body []byte
	var resp *http.Response
	var err error

	// Content-Type: application/x-www-form-urlencoded
	reqBody := "grant_type=urn:ietf:params:oauth:grant-type:jwt-bearer"
	reqBody += "&client_id=" + clientId
	reqBody += "&client_secret=" + clientSecret
	reqBody += "&assertion=" + jwtAssersion

	client := &http.Client{}
	r, _ := http.NewRequest("POST", "https://api.box.com/oauth2/token", bytes.NewBufferString(reqBody))
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	if resp, err = client.Do(r); err != nil {
		//fmt.Println(fmt.Sprintf("JwtRequestEntToken client.Do(r) err: %s\n", err))
		// if err set no need to close body
		return "", "", 0, err
	}
	// When err is nil, resp always contains a non-nil resp.Body.
	defer resp.Body.Close()

	if body, err = getResponse(resp); err != nil {
		//fmt.Println(fmt.Sprintf("JwtRequestEntToken err: %s\n", err))
		//fmt.Println(fmt.Sprintf("JwtRequestEntToken body: %s\n", body))
		return "", "", 0, err
	}

	// body is now byte array of what was returned
	// should look like the following:
	// {"access_token":"O0FxnzEkF1ph7UXXH8QIjYWzAXwOkMDX","expires_in":3720,"restricted_to":[],"token_type":"bearer"}
	//fmt.Println(fmt.Sprintf("JwtRequestEntToken status: %s\n", resp.Status))
	//fmt.Println(fmt.Sprintf("JwtRequestEntToken body: %s\n", body))

	type JwtTokenResponse struct {
		AccessToken string `json:"access_token"`
		ExpiresIn   int64  `json:"expires_in"`
		TokenType   string `json:"token_type"`
	}

	var jwtTokenResp JwtTokenResponse
	err = json.Unmarshal(body, &jwtTokenResp)
	if err != nil {
		//fmt.Println(fmt.Sprintf("err: %s\n", err))
		return "", "", 0, err
	}

	//fmt.Println("access_token: " + jwtTokenResp.AccessToken)
	//fmt.Println("  token_type: " + jwtTokenResp.TokenType)
	//fmt.Printf("  expires_in: %d\n", jwtTokenResp.ExpiresIn)

	return jwtTokenResp.AccessToken, jwtTokenResp.TokenType, jwtTokenResp.ExpiresIn, nil
}

func (b *Box) AuthCodeURL(state string) string {
	url := b.Config.AuthCodeURL(state, oauth2.AccessTypeOnline)
	return url
}

func (b *Box) Exchange(code string) error {
	//fmt.Printf("Exchange: code=%s", code)
	token, err := b.Config.Exchange(oauth2.NoContext, code)
	if err == nil {
		// grab a copy of the token negotiated
		b.StoreToken(token)
	}
	return err
}

func (b *Box) StoreToken(t *oauth2.Token) error {
	b.StoredToken = t
	//fmt.Println(fmt.Sprintf("StoreToken: {%s, %s, %s, %s}", t.TokenType, t.AccessToken, t.RefreshToken, t.Expiry.String()))
	return nil
}

func (b *Box) TokenIsSet() bool {
	if b.StoredToken != nil {
		return true
	}
	return false
}

func (b *Box) ExpireToken() {
	b.StoredToken.Expiry = time.Now()
}

func (b *Box) PrintTokenData() {
	fmt.Print("Token=(")
	if b.StoredToken != nil {
		fmt.Print("access_token: " + b.StoredToken.AccessToken)
		fmt.Print("    refresh_token: " + b.StoredToken.RefreshToken)
		fmt.Print("    expiry: " + b.StoredToken.Expiry.String())
	}
	fmt.Println(")")
}

func (b *Box) GetAccessToken() string {
	if b.StoredToken != nil {
		return b.StoredToken.AccessToken
	}
	return ""
}

func (b *Box) GetRefreshToken() string {
	if b.StoredToken != nil {
		return b.StoredToken.RefreshToken
	}
	return ""
}

func (b *Box) GetExpiry() string {
	if b.StoredToken != nil {
		return b.StoredToken.Expiry.String()
	}
	return ""
}

func (b *Box) GetApiUrl() string {
	return ApiURL
}

func (b *Box) Client() *http.Client {
	ctx := context.Background()
	var transport http.RoundTripper = &AuthTransport{b, http.DefaultTransport}
	ctx = context.WithValue(ctx, oauth2.HTTPClient, &http.Client{Transport: transport})
	client := b.Config.Client(ctx, b.StoredToken)
	return client
}

// doRequest performs the request (GET or POST) using authorized http
// client. You can also pass params to encode them in the request url
// or body to place in the request body.
// If login set (!= "") then On-Behalf-Of header is sent with that login id else ignored
func (b *Box) doRequest(method, path string, params *url.Values, reqBody []byte) ([]byte, error) {
	var body []byte
	var rawurl string
	var resp *http.Response
	var request *http.Request
	var err error
	var reqBodyReader io.Reader

	// The Box API only allows a max of 10 API requests per second.
	// To prevent ever hitting that adding a 2/10th sec delay here.
	time.Sleep(200 * time.Millisecond)

	//fmt.Println("doRequest: path="+path)

	if strings.HasPrefix(strings.ToLower(path), "http") {
		// then assume this is a fully qualified path
		rawurl = path
	} else {
		rawurl = fmt.Sprintf("%s/%s", ApiURL, path)
	}

	// If paramerters are nil then dont add `?` to the url
	if params != nil {
		rawurl = fmt.Sprintf("%s?%s", rawurl, params.Encode())
	}
	//fmt.Println(fmt.Sprintf("doRequest: method=%s  rawurl=%s\n", method, rawurl))

	// If reqBody is empty then dont create new reader
	if reqBody != nil {
		reqBodyReader = bytes.NewReader([]byte(reqBody))
	}

	if request, err = http.NewRequest(method, rawurl, reqBodyReader); err != nil {
		return nil, err
	}
	if b.SuppressBoxNotifications {
		request.Header.Set("Box-Notifications", "off")
	}
	if b.OnBehalfOf != "" {
		// this header uses the user login and is the old way of doing things
		//fmt.Println(fmt.Sprintf("doRequest On-Behalf-Of: %s\n", b.OnBehalfOf))
		request.Header.Set("On-Behalf-Of", b.OnBehalfOf)
	}
	if b.AsUser != "" {
		// this header uses the user id, this is the recomended way to make
		// requests on behalf of a user
		//fmt.Println(fmt.Sprintf("doRequest As-User: %s\n", b.AsUser))
		request.Header.Set("As-User", b.AsUser)
	}

	// dump the request details
	//dump, err := httputil.DumpRequest(request, true)
	//if err == nil {
	//	fmt.Println(fmt.Sprintf("doRequest request=%q", dump))
	//}

	if resp, err = b.Client().Do(request); err != nil {
		// if err set no need to close body
		return nil, err
	}
	// When err is nil, resp always contains a non-nil resp.Body.
	defer resp.Body.Close()
	if body, err = getResponse(resp); err != nil {
		//fmt.Println(fmt.Sprintf("doRequest err: %s\n", err))
		//fmt.Println(fmt.Sprintf("doRequest body: %s\n", body))
		return nil, fmt.Errorf("%s:%s", err, body)
	}
	return body, nil
}

func getResponse(r *http.Response) ([]byte, error) {
	var b []byte
	var err error
	if b, err = ioutil.ReadAll(r.Body); err != nil {
		return nil, err
	}

	err = toError(r.StatusCode)
	if err == SUCCESS ||
		err == CREATED ||
		err == ACCEPTED ||
		err == NON_AUTHORITATIVE_INFO ||
		err == NO_CONTENT ||
		err == RESET_CONTENT ||
		err == PARTIAL_CONTENT {
		return b, nil
	}
	return b, err // still returns b
}

func (this *Box) GetUserIdByLogin(userLogin string) (string, error) {
	identColl := &IdentityCollection{}
	err := identColl.GetUserIdByLogin(this, userLogin)
	if err != nil {
		return "", err
	}

	//fmt.Println(fmt.Sprintf("GetUserIdByLogin: Count: %d\n", identColl.Count))

	// now find the first matching login and return its id
	userId := ""
	identities := identColl.Entry
	for i := range identities {
		//fmt.Println(fmt.Sprintf("GetUserIdByLogin: Type: %s   Login: %s   Id: %s\n", identities[i].Type, identities[i].Login, identities[i].Id))
		// do case insensitive match - some box names have upper case
		if identities[i].Type == "user" && strings.ToLower(identities[i].Login) == strings.ToLower(userLogin) {
			userId = identities[i].Id
			break
		}
	}
	return userId, nil
}

func (this *Box) IsValidBoxUser(userLogin string) (bool, error) {

	// does this user exist in box enterprise?
	userId, err := this.GetUserIdByLogin(userLogin)
	if err != nil {
		return false, err
	}
	if userId == "" {
		return false, nil
	}
	return true, nil
}

func (this *Box) GetGroupIdByName(groupName string) (string, error) {
	identColl := &IdentityCollection{}
	err := identColl.GetGroupIdByName(this, groupName)

	//fmt.Println(fmt.Sprintf("GetGroupIdByName: Count: %d\n", identColl.Count))

	// now find the first matching name and return its id
	groupId := ""
	identities := identColl.Entry
	for i := range identities {
		//fmt.Println(fmt.Sprintf("GetGroupIdByName: Type: %s   Name: %s   Id: %s\n", identities[i].Type, identities[i].Name, identities[i].Id))
		if identities[i].Type == "group" && strings.ToLower(identities[i].Name) == strings.ToLower(groupName) {
			groupId = identities[i].Id
			break
		}
	}
	return groupId, err
}

func (this *Box) GetProjectDirectoryUsers() ([]Identity, error) {

	var prjDirs []Identity
	var limit int = 1000

	// First lookup the "Prj_" names
	var offset int = 0
	done := false
	for !done {
		identColl := &IdentityCollection{}

		//fmt.Println(fmt.Sprintf("project_ Calling GetUsersFilterdBy with limit: %d, offset: %d", limit, offset))
		err := identColl.GetUsersFilteredBy(this, "prj_", limit, offset)
		if err != nil {
			//fmt.Sprintf("GetUsersFilteredBy(%s) failed with %s\n", "project_", err)
			return nil, err
		}

		identities := identColl.Entry
		for i := range identities {
			//fmt.Println(fmt.Sprintf("GetProjectDirectoryUsers:  Id: %s Name: %s Login: %s\n", identities[i].Id, identities[i].Name, identities[i].Login))
			prjDirs = append(prjDirs, identities[i])
		}

		//fmt.Println(fmt.Sprintf("project_ GetUsersFilterdBy returned with identColl.Count: %d,  len(identColl.Entry): %d, offset: %d", identColl.Count, len(identColl.Entry), offset))
		total_count := identColl.Count // this is the total number of users that matched the query
		count := len(identColl.Entry)  // this is the number of records returned in this block
		offset += count

		if count == 0 || count != limit || offset == total_count {
			// We're done if:
			// 1. (count == 0)            ==> we got nothing this fetch
			// 2. (count != limit)        ==> we got less than a max size block
			// 3. (offset == total_count) ==> we have all the records that matched the query
			//fmt.Println(fmt.Sprintf("project_ GetUsersFilterdBy DONE total_count: %d,  len(identColl.Entry): %d, limit: %d, offset: %d", identColl.Count, count, limit, offset))
			done = true
		}
	}

	// Next lookup the "Project_" names
	offset = 0
	done = false
	for !done {
		identColl := &IdentityCollection{}

		//fmt.Println(fmt.Sprintf("group_ Calling GetUsersFilterdBy with limit: %d, offset: %d", limit, offset))
		err := identColl.GetUsersFilteredBy(this, "project_", limit, offset)
		if err != nil {
			//fmt.Sprintf("GetUsersFilteredBy(%s) failed with %s\n", "project_", err)
			return nil, err
		}

		identities := identColl.Entry
		for i := range identities {
			//fmt.Println(fmt.Sprintf("GetProjectDirectoryUsers:  Id: %s Name: %s Login: %s\n", identities[i].Id, identities[i].Name, identities[i].Login))
			prjDirs = append(prjDirs, identities[i])
		}

		//fmt.Println(fmt.Sprintf("group_ GetUsersFilterdBy returned with identColl.Count: %d,  len(identColl.Entry): %d, offset: %d", identColl.Count, len(identColl.Entry), offset))
		total_count := identColl.Count // this is the total number of users that matched the query
		count := len(identColl.Entry)  // this is the number of records returned in this block
		offset += count

		if count == 0 || count != limit || offset == total_count {
			// We're done if:
			// 1. (count == 0)            ==> we got nothing this fetch
			// 2. (count != limit)        ==> we got less than a max size block
			// 3. (offset == total_count) ==> we have all the records that matched the query
			//fmt.Println(fmt.Sprintf("group_ GetUsersFilterdBy DONE total_count: %d,  len(identColl.Entry): %d, limit: %d, offset: %d", identColl.Count, count, limit, offset))

			done = true
		}
	}

	// Next lookup the "Group_" names
	offset = 0
	done = false
	for !done {
		identColl := &IdentityCollection{}

		//fmt.Println(fmt.Sprintf("group_ Calling GetUsersFilterdBy with limit: %d, offset: %d", limit, offset))
		err := identColl.GetUsersFilteredBy(this, "group_", limit, offset)
		if err != nil {
			//fmt.Sprintf("GetUsersFilteredBy(%s) failed with %s\n", "group_", err)
			return nil, err
		}

		identities := identColl.Entry
		for i := range identities {
			//fmt.Println(fmt.Sprintf("GetProjectDirectoryUsers:  Id: %s Name: %s Login: %s\n", identities[i].Id, identities[i].Name, identities[i].Login))
			prjDirs = append(prjDirs, identities[i])
		}

		//fmt.Println(fmt.Sprintf("group_ GetUsersFilterdBy returned with identColl.Count: %d,  len(identColl.Entry): %d, offset: %d", identColl.Count, len(identColl.Entry), offset))
		total_count := identColl.Count // this is the total number of users that matched the query
		count := len(identColl.Entry)  // this is the number of records returned in this block
		offset += count

		if count == 0 || count != limit || offset == total_count {
			// We're done if:
			// 1. (count == 0)            ==> we got nothing this fetch
			// 2. (count != limit)        ==> we got less than a max size block
			// 3. (offset == total_count) ==> we have all the records that matched the query
			//fmt.Println(fmt.Sprintf("group_ GetUsersFilterdBy DONE total_count: %d,  len(identColl.Entry): %d, limit: %d, offset: %d", identColl.Count, count, limit, offset))

			done = true
		}
	}

	return prjDirs, nil
}

func (this *Box) GetFolderCollaborations(folderid string) (*FolderCollaborations, error) {
	folderCollaborations := &FolderCollaborations{}
	err := folderCollaborations.Get(this, folderid)
	return folderCollaborations, err
}

func (this *Box) GetFolders(path string) (*http.Response, error) {
	client := this.Client()
	rawurl := fmt.Sprintf("%s/%s", ApiURL, url.QueryEscape(path))
	folders, err := client.Get(rawurl)
	return folders, err
}

func (this *Box) GetFolder(id string) (*Folder, error) {
	folder := &Folder{Id: id}
	err := folder.Get(this)
	return folder, err
}

func (this *Box) GetGroups() (*IdentityCollection, error) {
	identColl := &IdentityCollection{}
	err := identColl.Groups(this)
	return identColl, err
}

func (this *Box) GetGroup(id string) (*Group, error) {
	group := &Group{Id: id}
	err := group.Get(this)
	return group, err
}

func (this *Box) GetUser(id string) (*User, error) {
	u := &User{Id: id}
	err := u.Get(this)
	return u, err
}

func (this *Box) SetUserQuota(id string, q float64) (*User, error) {
	u := &User{Id: id, SpaceAmount: q}
	err := u.Update(this)
	return u, err
}

func (this *Box) SetUserQuotaUnlimited(id string) (*User, error) {
	u := &User{Id: id, SpaceAmount: -1}
	err := u.Update(this)
	return u, err
}

func (this *Box) GetGroupMemberships(groupId string) (*MembershipCollection, error) {
	mbrColl := &MembershipCollection{}
	count := 1 // need some non-zero starting point
	limit := 200
	for i := 0; i < count; i += limit {

		//fmt.Printf("  Loop: GrpId=%s, count=%d, limit=%d, offset=%d\n", groupId, count, limit, i)
		mc := &MembershipCollection{}
		mc.Offset = i
		mc.Limit = limit
		err := mc.GetMembershipsForGroup(this, groupId)
		if err != nil {
			return nil, err
		}
		count = mc.Count

		//fmt.Printf(" Fetch: GrpId=%s, len(Entry)=%d, Count=%d, Limit=%d, Offset=%d\n", groupId, len(mc.Entry), mc.Count, mc.Limit, mc.Offset)

		// now add this fetch to the return structure
		mbrColl.Count = mc.Count
		mbrColl.Offset = 0
		mbrColl.Limit = limit
		for _, m := range mc.Entry {
			mbrColl.Entry = append(mbrColl.Entry, m)
		}
	}

	//fmt.Printf("Return: GrpId=%s, len(Entry)=%d, Count=%d, Limit=%d, Offset=%d\n", groupId, len(mbrColl.Entry), mbrColl.Count, mbrColl.Limit, mbrColl.Offset)
	return mbrColl, nil
}

func (this *Box) GetMembershipIdByUserIdGroupId(userId string, groupId string) (string, error) {
	membershipId := ""
	mbrColl, err := this.GetGroupMemberships(groupId)
	if err != nil {
		//fmt.Printf("folder.Items() failed with '%s'\n", err)
		return "", err
	} else {
		recs := mbrColl.Entry
		for i := range recs {
			if recs[i].Type == "group_membership" && recs[i].User.Id == userId {
				// found match, return the membershipId
				membershipId = recs[i].Id
				break
			}
		}
	}
	return membershipId, err
}

func (this *Box) GetMembership(id string) (*Membership, error) {
	mbr := &Membership{Id: id}
	err := mbr.Get(this)
	return mbr, err
}

func (this *Box) CreateGroupMembership(groupId string, userId string) (*Membership, error) {
	mbr := &Membership{Group: &Identity{Id: groupId}, User: &Identity{Id: userId}}
	err := mbr.CreateGroupMembership(this)
	return mbr, err
}

func (this *Box) CreateGroupMembershipGivenLogin(groupId string, userLogin string) (*Membership, error) {
	mbr := &Membership{Group: &Identity{Id: groupId}, User: &Identity{Login: userLogin}}
	err := mbr.CreateGroupMembership(this)
	return mbr, err
}

func (this *Box) RemoveGroupMembership(membershipId string) (*Membership, error) {
	mbr := &Membership{Id: membershipId}
	err := mbr.RemoveGroupMembership(this)
	return mbr, err
}

func (this *Box) CreateGroup(groupName string) (*Group, error) {
	group := &Group{Name: groupName}
	err := group.CreateGroup(this)
	return group, err
}

func (this *Box) DeleteGroup(groupId string) (*Group, error) {
	group := &Group{Id: groupId}
	err := group.DeleteGroup(this)
	return group, err
}

func (this *Box) CreateEnterpriseUser(login, name string) (*User, error) {
	usr := &User{Login: login, Name: name, Role: "user"}
	err := usr.CreateUser(this)
	return usr, err
}

func (this *Box) CreateRootFolder(dir_name string) (*Folder, error) {
	parent_fld := &Folder{Id: "0"}                    // parent folder id is only field needed, returns created folder
	sub_fld, err := parent_fld.Create(this, dir_name) // pass in name of sub folder to create
	return sub_fld, err
}

func (this *Box) CreateCollaborationAddRoleToFolder(project_directory_folder_id, new_coowner_user_id, role string) (*Collaboration, error) {

	collab := &Collaboration{
		Item: &Entity{
			Id:   project_directory_folder_id,
			Type: "folder",
		},
		AccessibleBy: &Identity{
			Id:   new_coowner_user_id,
			Type: "user",
		},
		Role:   role,
		Notify: false,
	}

	err := collab.CreateCollaboration(this)
	return collab, err
}

func (this *Box) DeleteCollaboration(collabId string) (*Collaboration, error) {
	collab := &Collaboration{Id: collabId}
	err := collab.DeleteCollaboration(this)
	return collab, err
}
