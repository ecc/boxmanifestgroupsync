package box

type Collection struct {
	Count  int      `json:"total_count,omitempty"`
	Entry  []Entity `json:"entries,omitempty"`
	Limit  int      `json:"limit,omitempty"`
	Offset int      `json:"offset,omitempty"`
}
