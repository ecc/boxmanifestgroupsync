package box

import (
	"encoding/json"
	"errors"
	"fmt"
)

type Membership struct {
	Type       string    `json:"type,omitempty"`        // Type for membership is 'group_membership'
	Id         string    `json:"id,omitempty"`          // Box’s unique string identifying this membership
	User       *Identity `json:"user,omitempty"`        // Mini representation of the user, including id and name of user.
	Group      *Identity `json:"group,omitempty"`       // Mini representation of the group, including id and name of group.
	Role       string    `json:"role,omitempty"`        // The role of the user in the group. Default is “member” with option for “admin”
	CreatedAt  *BoxTime  `json:"created_at,omitempty"`  // The time this membership was created.
	ModifiedAt *BoxTime  `json:"modified_at,omitempty"` // The time this membership was last modified

}

// Get populates the fields of the struct.
// Only Id is required apriori.
func (this *Membership) Get(box *Box) error {
	if this.Id == "" {
		return errors.New("Empty id while using GetMembershipsForGroup")
	}
	rawurl := fmt.Sprintf("group_memberships/%s", this.Id)
	fmt.Println("membership.Get: rawurl=" + rawurl)
	body, err := box.doRequest("GET", rawurl, nil, nil)

	if err == nil {
		err = json.Unmarshal(body, this)
		return err
	}
	return err
}

// The group id and the user id must be set in this object before calling
func (this *Membership) CreateGroupMembership(box *Box) error {

	// the user id and the group id must be set
	reqBody, _ := json.Marshal(this)

	body, err := box.doRequest("POST", "group_memberships", nil, reqBody)
	if err == nil {
		err = json.Unmarshal(body, this)
		return err
	}
	return err
}

// The group id and the user id must be set in this object before calling
func (this *Membership) RemoveGroupMembership(box *Box) error {
	rawurl := fmt.Sprintf("group_memberships/%s", this.Id)
	_, err := box.doRequest("DELETE", rawurl, nil, nil)
	return err
}
