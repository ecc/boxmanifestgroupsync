package box

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/astaxie/beego"
)

type FolderCollaborations struct {
	Count   int             `json:"total_count,omitempty"`
	Entries []Collaboration `json:"entries,omitempty"`
	Limit   int             `json:"limit,omitempty"`
	Offset  int             `json:"offset,omitempty"`
}

// Look up folder collaborations
// Note that only Id (folder id) is required apriori.
func (f *FolderCollaborations) Get(box *Box, folderId string) error {
	if folderId == "" {
		return errors.New("Empty folderId while using Get")
	}
	rawurl := fmt.Sprintf("folders/%s/collaborations", folderId)
	beego.Debug(fmt.Sprintf("FolderCollaborations.Get: rawurl=" + rawurl))
	body, err := box.doRequest("GET", rawurl, nil, nil)

	if err == nil {
		err = json.Unmarshal(body, f)
		return err
	}
	return err
}
