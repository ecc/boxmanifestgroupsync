package box

import (
	"encoding/json"
	"errors"
	"fmt"
)

type MembershipCollection struct {
	Count  int          `json:"total_count,omitempty"`
	Entry  []Membership `json:"entries,omitempty"`
	Limit  int          `json:"limit,omitempty"`
	Offset int          `json:"offset,omitempty"`
}

// Get populates the fields of the struct.
// groupId, Limit, and Offset required apriori.
func (this *MembershipCollection) GetMembershipsForGroup(box *Box, groupId string) error {
	if groupId == "" {
		return errors.New("Empty id while using GetMembershipsForGroup")
	}

	// make sure default is set
	if this.Limit == 0 {
		this.Limit = 100
	}

	rawurl := fmt.Sprintf("groups/%s/memberships?limit=%d&offset=%d", groupId, this.Limit, this.Offset)

	//fmt.Println("MembershipCollection.GetMembershipsForGroup: rawurl=" + rawurl)
	body, err := box.doRequest("GET", rawurl, nil, nil)
	//fmt.Println("MembershipCollection.GetMembershipsForGroup: body=" + string(body[:]))
	if err == nil {
		err = json.Unmarshal(body, this)
		return err
	}
	return err
}
