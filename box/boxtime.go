package box

import (
	"encoding/json"
	"time"
)

type BoxTime time.Time

// UnmarshalJSON unmarshals a time according to the Dropbox format.
func (bt *BoxTime) UnmarshalJSON(data []byte) error {
	if data == nil || string(data) == "null" {
		return nil
	}
	var s string
	var err error
	var t time.Time
	if err = json.Unmarshal(data, &s); err != nil {
		return err
	}
	if t, err = time.ParseInLocation(time.RFC3339, s, time.UTC); err != nil {
		return err
	}
	if t.IsZero() {
		*bt = BoxTime(time.Time{})
	} else {
		*bt = BoxTime(t)
	}
	return nil
}

// MarshalJSON marshals a time according to the Dropbox format.
func (bt BoxTime) MarshalJSON() ([]byte, error) {
	return json.Marshal(time.Time(bt).Format(time.RFC3339))
}
