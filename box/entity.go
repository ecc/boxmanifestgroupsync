package box

import (
	"errors"
)

//  Represents both mini folder and mini file.
type Entity struct {
	SequenceId string `json:"sequence_id,omitempty"` // A unique ID for use with the /events endpoint.
	Name       string `json:"name,omitempty"`        // The name of the entity.
	Id         string `json:"id,omitempty"`          // The id of the entity.
	ETag       string `json:"etag,omitempty"`        // A unique string identifying the version of this entity.
	Type       string `json:"type,omitempty"`        // Type of entity
}

// IsFolder checks if the given entity is a folder
func (this *Entity) IsFolder() bool {
	if this.Type == "folder" {
		return true
	} else {
		return false
	}
}

// toFolder converts the given entity to a folder. Only attributes present in
// the entity are populated rest are untouched.
func (this *Entity) toFolder(f *Folder) error {
	if !this.IsFolder() {
		return errors.New("Entity is not a folder")
	}
	f.Id = this.Id
	f.Name = this.Name
	f.ETag = this.ETag
	f.SequenceId = this.SequenceId
	return nil
}

// IsFile checks if the given entity is a file.
func (this *Entity) IsFile() bool {
	if this.Type == "file" {
		return true
	} else {
		return false
	}
}

// toFile converts the given entity to a file. Only attributes present in
// the entity are populated rest are untouched.
func (this *Entity) toFile(f *File) error {
	if !this.IsFile() {
		return errors.New("Entity is not a file")
	}
	f.Id = this.Id
	f.Name = this.Name
	f.ETag = this.ETag
	f.SequenceId = this.SequenceId
	return nil
}
